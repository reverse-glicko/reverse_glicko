#! /usr/bin/python3

import math, random
import scipy.optimize

G_TAU=0.2
VERBOSE=0

def dprint(*x):
    if VERBOSE==1:
        s="".join(map(str,x))
        print(s)

#
# Throughout, 'rating' and 'rating deviation (RD)' are in the Glicko
# units, while 'mu' denotes rating in Glicko-2 units, and 'phi'
# denotes rating deviation in Glicko-2 units
#

class Player:
    def __init__(self, rating=1500, rd=360, volatility=0.06):
        self.volatility=volatility
        self.rd=rd
        self.rating=rating

    def g(phi):
        return 1.0/math.sqrt(1+(3*phi*phi)/(math.pi*math.pi))

    def E(mu, muj, phij):
        g = Player.g
        return 1.0/(1.0+math.exp(-g(phij)*(mu-muj)))

    def illinois(f, start, end, eps, imax):
        fstart = f(start)
        fend = f(end)
        i = 0
        side = 0
        while i < imax:
            #print("start ", start, " end ", end, " fstart ", fstart, " fend ", fend)
            r = (fstart*end-fend*start)/(fstart-fend)
            if abs(end-start) < eps*abs(start+end):
                break
            fr = f(r)
            if fr*fend > 0:
                end=r
                fend=fr
                if side==-1:
                    fstart=fstart/2
                side = -1
            elif fstart*fr > 0:
                start=r
                fstart=fr
                if side==1:
                    fend=fend/2
                side=1
            else:
                break
        return r
            

    def outcome(self, matches):
        E=Player.E
        g=Player.g
        mu = (self.rating-1500)/173.7178
        phi = self.rd/173.7178
        #print("mu ", mu, " phi ", phi)
        nr=len(matches)
        mu_opp = []
        phi_opp = []
        s_opp = []
        for (opponent, score) in matches:
            mu_opp.append((opponent.rating-1500)/173.7178)
            phi_opp.append((opponent.rd/173.7178))
            s_opp.append(score)
        for j in range(nr):
            dprint("j ", j, " muj ", mu_opp[j], " phij ", phi_opp[j], " g(phij) ", g(phi_opp[j]), " E() ", E(mu, mu_opp[j], phi_opp[j]), " sj ", s_opp[j])
        v = 0.0
        for j in range(nr):
            muj = mu_opp[j]
            phij = phi_opp[j]
            v += g(phij)**2 * E(mu,muj,phij) * (1-E(mu, muj, phij))
        v = 1.0/v
        dprint("v = ", v)
        delta = 0.0
        for j in range(nr):
            muj = mu_opp[j]
            phij = phi_opp[j]
            sj = s_opp[j]
            delta += g(phij)*(sj-E(mu, muj, phij))
        delta = v * delta
        dprint("delta ", delta)
        a = math.log(self.volatility*self.volatility)
        f = lambda x: ((math.exp(x)*(delta**2-phi**2-v-math.exp(x))) / (2*(phi**2+v+math.exp(x))**2)) - ((x-a)/G_TAU**2)
        start = a
        if delta**2 > phi**2+v:
            end = math.log(delta**2-phi**2-v)
        else:
            k = 1
            while f(a-k*G_TAU) < 0:
                k += 1
            end = a-k*G_TAU
        #print("start ", start, " end ", end)
        val = Player.illinois(f, start, end, 0.000001, 20)
        #print("illinois ", val)
        self.volatility = math.exp(val/2)
        phistar = math.sqrt(phi**2+self.volatility**2)
        #print("phistar ", phistar)
        phi_new = 1.0/math.sqrt(1.0/phistar**2+1.0/v)
        #print("phi_new ", phi_new)
        mu_new = 0.0
        for j in range(nr):
            muj = mu_opp[j]
            phij = phi_opp[j]
            sj = s_opp[j]
            mu_new += g(phij)*(sj-E(mu, muj, phij))
            dprint("sj =",sj," g = ", g(phij), " E=", E(mu,muj,phij))
        mu_new = mu + phi_new**2 * mu_new
        dprint("mu_new ", mu_new, " change ", mu_new-mu)
        self.rating = 173.7178*mu_new+1500
        self.rd = 173.7178*phi_new
        #print("rating ", self.rating, " RD ", self.rd)

    def probability(self, opponent):
        g = Player.g
        E = Player.E
        mu1 = (self.rating-1500)/173.7178
        mu2 = (opponent.rating-1500)/173.7178
        phi1 = self.rd/173.7178
        phi2 = opponent.rd/173.7178
        pr = E(mu1, mu2, math.sqrt(phi1**2+phi2**2))
        return pr


def r2d2(rd1, rd2):
    a = Player(1601.7, rd1)
    b = Player(1360.0, rd2)
    a.outcome([(b, 1)])
    return a.rating - 1611.0

def find_own_rating(own, their, win_lose, result):
    def f(x):
        a = Player(own, x)
        b = Player(their)
        a.outcome([(b, win_lose)])
        return a.rating - result
    v = Player.illinois(f, 0, 350, 0.00001, 20)
    print("illinois method RD=", v, " delta ", f(v))
    return v

def test():
    # glicko-2 example uses tau of 0.5
    G_TAU=0.5
    a = Player(1500, 200, 0.06)
    b1 = Player(1400, 30)
    b2 = Player(1550, 100)
    b3 = Player(1700, 300)

    # glicko example
    a.outcome([(b1, 1), (b2, 0), (b3, 0)])

league_matches_12_14=[(Player(2030, 150), 0),
                      (Player(1870, 140), 0),
                      (Player(1620, 130), 1),
                      (Player(1880, 120), 0),
                      (Player(1860, 110), 0),
                      (Player(1880, 100), 1),
                      (Player(1810, 90), 0)]

matches_12_17=[
    (None, 2120, 0, None),
    (None, 1840, 0, None),
    (None, 1680, 0, None),
    (None, 1900, 0, None),
    (None, 1930, 0, None),
    (None, 1930, 0, None),
    (None, 1430, 1, None),
    (1664.5, 1520 , 0, -53.9),
    (1610.6, 1660 , 0, -31.4),
    (1579.2, 1460 , 1, 22.7),
    (1601.9, 1690 , 1, 39.0),
    (1640.0, 1630 , 1, 29.0),
    (1669.9, 1620 , 0, -32.2),
    (1637.7, 1660 , 0, -25.5),
    (1612.2, 1660 , 1, 28.5),
    (1640.7, 1470 , 0, -33.0),
    (1607.7, 1710 , 0, -16.7),
    (1591.0, 1380 , 1, 10.7),
    (1601.7, 1360 , 1, 9.3)]

def single_rating_period(matches):
    a = Player()
    a.outcome(league_matches_12_14)
    print("Final power level ", a.rating, " RD ", a.rd)

def rating_replay(initial):
    a = Player(1900)
    for m in league_matches_12_14:
        (opp, result) = m
        pr = a.probability(opp)
        print("rating ", a.rating, " opponent ", opp.rating, " win prob ", pr)
        a.outcome([m])
        print("updated rating ", a.rating, " rd = ", a.rd)

def gnuplot_r2d2():
    for rd1 in range(-350, 350,5):
        for rd2 in range(-350, 350,5):
            v = r2d2(rd1, rd2)
            print("%f %f %f" % (rd1, rd2, v))
        print()

def replay_matches(player, matches):
    for (_, opp_rating, wl, _) in matches:
        me.outcome([(Player(opp_rating), wl)])
        print("new rating ", me.rating, " rd ", me.rd)

def replay_matches_2(player, matches):
    f = open("sim.txt", "w")
    for i in range(len(matches)):
        p = Player(player.rating, player.rd) # make a copy, actually
        opp_rd = 300-i*0
        m2 = [(Player(opp, opp_rd), wl) for (_, opp, wl, _) in matches[0:i+1]]
        p.outcome(m2)
        print(f'round {i} player status {p.rating:.1f}, {p.rd:.1f}' + (f' expected {matches[i][0]+matches[i][3]:.1f}' if matches[i][0] is not None else ""))
        f.write(str(p.rating)+"\n")
    f.close()

def optimize_me(player, matches, opp_rd):
    result = 0
    for i in range(len(matches)):
        p = Player(player.rating, player.rd) # make a copy, actually
        m2 = [(Player(opp, opp_rd), wl) for ((_, opp, wl, _), opp_rd) in zip(matches[0:i+1], opp_rd[0:i+1])]
        p.outcome(m2)
        if matches[i][0] is not None:
            expected = matches[i][0]+matches[i][3]
            error = expected - p.rating
            result += error*error
    return result

def monte_carlo(matches):
    minerr = 9999999
    best_player=None
    best_opp_rd=None
    while True:
        rating = random.uniform(1500, 2000)
        rd = random.uniform(0, 350)
        opp_rd = [random.uniform(0, 350) for x in range(len(matches))]
        p = Player(rating, rd)
        p2 = Player(rating, rd)
        err = optimize_me(p, matches, opp_rd)
        if err < minerr:
            best_player=p2
            best_opp_rd=opp_rd
            minerr = err
            print(f'new best: error={minerr} player={p2.rating},{p2.rd}, opp_rd={opp_rd}')

def plot_matches(player, opp_rd, matches):
    f = open("plot.txt", "w")
    f.write('set datafile missing "?"\nset style data lines\n$data << e\n')
    for i in range(len(matches)):
        p = Player(player.rating, player.rd) # make a copy, actually
        plot_rd = opp_rd.copy()
        m2 = [(Player(opp, opp_rd), wl) for ((_, opp, wl, _), opp_rd) in zip(matches[0:i+1], opp_rd[0:i+1])]
        p.outcome(m2)
        f.write(str(p.rating)+" ")
        if matches[i][0] is None:
            f.write(" ? ")
        else:
            f.write(str(matches[i][0]+matches[i][3]))
        f.write("\n")
    f.write('e\nplot $data using 1 title "Simulated", $data using 2 title "Actual"\n')
    f.close()

def use_scipy(matches):
    def f(x, args):
        p = Player(x[0], x[1])
        opp_rd = x[2:]
        return optimize_me(p, matches, opp_rd)

    rating = random.uniform(1500, 2000)
    rd = random.uniform(0, 350)
    opp_rd = [random.uniform(0, 350) for x in range(len(matches))]
    bounds = [(1500,2000), (0, 350)] + [(0,350)]*len(matches)
    x0 = [rating, rd] + opp_rd
    res = scipy.optimize.minimize(f, x0, matches, bounds=bounds)
    x = res['x']
    #x = [1768.61815403,  155.84070799,   45.22250923,  350.,          350.,
    #     350.,          350.,          350.,          266.96749095,  350.,
    #     171.32132263,    0.,            0.,          153.85473928,  135.52123047,
    #     99.16868066,   44.558592,    144.5267656,   212.85421681,  249.43281987,
    #     292.46239755]
    print("res = ", x)
    p = Player(x[0], x[1])
    opp_rd = x[2:]
    plot_matches(p, opp_rd, matches)
    print(f"Initial rating {x[0]} RD {x[1]}")
    print(f"Optimized error function value: {res['fun']}")
# what to do?

# single_rating_period(league_matches_12_14)
# rating_replay(1900)
#rd = find_own_rating(1664.5, 1520, 0, 1610.6)
#me = Player(1664.5, rd)
#replay_matches(me, matches_12_17[7:])

#me = Player(1990, 240)
#replay_matches_2(me, matches_12_17)

# gnuplot_r2d2()

# optimize one?
#opp_rd_estimate=[150]*len(matches_12_17)
#me = Player(1990, 240)
#optimize_me(me, matches_12_17, opp_rd_estimate)

# play at the casino
# monte_carlo(matches_12_17)
use_scipy(matches_12_17)
